package com.app.kinna.interviewquepaper.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.app.kinna.interviewquepaper.R
import com.app.kinna.interviewquepaper.adapter.ImageAdapter
import com.app.kinna.interviewquepaper.model.ImageListModel
import com.app.kinna.interviewquepaper.utils.APIService
import com.app.kinna.interviewquepaper.utils.RestClient
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var mApiService: APIService
    private var mAdapter: ImageAdapter? = null
    private var mQuestions: List<ImageListModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadData()
    }

    private fun loadData() {
        mApiService = RestClient.client.create(APIService::class.java)
        listRecyclerView!!.layoutManager = GridLayoutManager(this, 2)
        fetchImageList()
    }

    private fun fetchImageList() {
        val call = mApiService.fetchImageData()
        call.enqueue(object : Callback<List<ImageListModel>> {
            override fun onResponse(call: Call<List<ImageListModel>>, response: Response<List<ImageListModel>>) {
                val imageData = response.body()
                if (imageData != null) {
                    mQuestions = ArrayList(imageData)
                    mAdapter = ImageAdapter(applicationContext, mQuestions)
                    listRecyclerView!!.adapter = mAdapter
                    mAdapter!!.notifyDataSetChanged()
                }
            }
            override fun onFailure(call: Call<List<ImageListModel>>, t: Throwable) {
                Log.e(TAG, "Got error : " + t.localizedMessage)
            }
        })
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
}

