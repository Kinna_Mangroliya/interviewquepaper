package com.app.kinna.interviewquepaper.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.kinna.interviewquepaper.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.activity_image_preview.*

class ImagePreviewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_preview)
        getData()
    }
    private fun getData() {
        val bundle: Bundle? = intent.extras
        val imageDataItem = bundle!!.get("image")
        val title:String = bundle.get("title") as String

        Glide.with(this)
            .load(imageDataItem)
            .placeholder(R.mipmap.ic_launcher)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(iv_image_preview)
        tv_author_name.text = title
    }
}
