package com.app.kinna.interviewquepaper.utils

import com.app.kinna.interviewquepaper.model.ImageListModel
import retrofit2.Call
import retrofit2.http.GET

interface APIService {
    @GET("/list")
    fun fetchImageData(): Call<List<ImageListModel>>
}