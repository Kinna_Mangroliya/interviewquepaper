package com.app.kinna.interviewquepaper.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.kinna.interviewquepaper.R
import com.app.kinna.interviewquepaper.activity.ImagePreviewActivity
import com.app.kinna.interviewquepaper.model.ImageListModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.list_image_item.view.*

class ImageAdapter(
    private val context: Context,
    private val mQuestions: List<ImageListModel>? = listOf()
) : RecyclerView.Adapter<ImageAdapter.QuestionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_image_item, parent, false)
        return QuestionViewHolder(view)
    }

    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
        val imageDataItem = "https://picsum.photos/300/300?image=${mQuestions!![position].id}"
        val authorName=mQuestions[position].author

        Glide.with(context)
            .load(imageDataItem)
            .placeholder(R.mipmap.ic_launcher)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(holder.ivImage)
        holder.tvAuthorName.text = authorName

        holder.containerView.setOnClickListener {

            context.startActivity(
                Intent(context, ImagePreviewActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra("image", imageDataItem)
                    .putExtra("title", authorName)
            )
        }
    }

    override fun getItemCount(): Int {
        return mQuestions!!.size
    }

    class QuestionViewHolder(val containerView: View) : RecyclerView.ViewHolder(containerView) {
        val ivImage:ImageView = containerView.iv_image
        val tvAuthorName:TextView = containerView.tv_author_name
    }
}